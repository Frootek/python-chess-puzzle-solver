
import cv2
import numpy as np

class LichessImageAnalyzer():
    def __init__(self, image_path):
        self.pieces = ['P', 'R', 'N', 'B','Q', 'K',
                    'p_', 'r_', 'n_', 'b_','q_', 'k_']
        
        self.current_bottom_side = 'W'

        self.image_path = image_path
        self.fen = ''
    
        self.white_to_move = ' w - - 0 1'
        self.black_to_move = ' b - - 0 1'
        self.side_to_move = self.white_to_move

        ##standard FEN notation while E represents empty square     
        self.chess_board = [
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E']
        ]
        pieces_len = len(self.pieces)

        for i in range(pieces_len):

            og_image = cv2.imread(image_path)

            resize_dim = (600,600)

            img_rgb = cv2.resize(og_image, resize_dim)

            img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)

            sqr_w, sqr_h = resize_dim[0] // 8, resize_dim[1] // 8

            piece = self.pieces[i]

            piece_file_loc = 'pieces-small-scaled/{}.JPG'.format(piece)

            template = cv2.imread(piece_file_loc,0)

            w, h = sqr_w, sqr_h
            res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)

            threshold = 0.80
            loc = np.where(res >= threshold)


            for pt in zip(*loc[::-1]):
                #looking for upper left corner of each square

                position_x = int((pt[0] // sqr_w) * sqr_w)
                position_y = int((pt[1] // sqr_h) * sqr_h)

                x_y_start = (position_x,position_y)
                x_y_end = (position_x + int(sqr_w), position_y + int(sqr_h))

                board_x = position_x//sqr_w
                board_y = position_y//sqr_h

                self.chess_board[board_y][board_x] = piece[0]
        
            self.board_to_fen()

    def board_to_fen(self):
        empty_squares = 0
        fen = ''
        chess_board = self.chess_board
        ##board is 2d chessboard, using fen notation and  E displays square
        for row in chess_board:
            for square in row:
                if (square == 'E'):
                    empty_squares+=1
                    continue
                else:
                    if (empty_squares != 0):
                        fen += str(empty_squares)
                        empty_squares = 0
                    fen += square
                
            if (empty_squares  != 0):
                fen += str(empty_squares)
                empty_squares = 0
            fen += '/'

        fen = fen[:-1]

        fen += self.side_to_move
        
        self.fen = fen

    
    def flip_fen(self):        
        split_fen = self.fen.split()[0]
        fliped_fen = split_fen[::-1]
        

        if self.current_bottom_side == 'W':
            self.current_bottom_side = 'B'
            self.side_to_move = self.black_to_move

        else :
            self.current_bottom_side = 'W'
            self.side_to_move = self.white_to_move

        self.fen = fliped_fen + self.side_to_move

    def get_fen_notation(self):
        return self.fen

    def get_image_path(self):
        return self.image_path

class ChessComImageAnalyzer():
    def __init__(self, image_path):
        self.pieces = ['P', 'R', 'N', 'B','Q', 'K',
                    'p_', 'r_', 'n_', 'b_','q_', 'k_']
        
        self.current_bottom_side = 'W'

        self.image_path = image_path
        self.fen = ''
    
        self.white_to_move = ' w - - 0 1'
        self.black_to_move = ' b - - 0 1'
        self.side_to_move = self.white_to_move

        ##standard FEN notation while E represents empty square     
        self.chess_board = [
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E'],
            ['E','E','E','E','E','E','E','E']
        ]
        pieces_len = len(self.pieces)

        for i in range(pieces_len):

            og_image = cv2.imread(image_path)

            resize_dim = (600,600)

            img_rgb = cv2.resize(og_image, resize_dim)

            img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)

            sqr_w, sqr_h = resize_dim[0] // 8, resize_dim[1] // 8

            piece = self.pieces[i]

            piece_file_loc_white_bg = 'chess-com/chess-com-pieces/{}_W.JPG'.format(piece)

            template = cv2.imread(piece_file_loc_white_bg,0)

            w, h = sqr_w, sqr_h
            res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)

            threshold = 0.80
            loc = np.where(res >= threshold)


            for pt in zip(*loc[::-1]):
                #looking for upper left corner of each square

                position_x = int((pt[0] // sqr_w) * sqr_w)
                position_y = int((pt[1] // sqr_h) * sqr_h)

                x_y_start = (position_x,position_y)
                x_y_end = (position_x + int(sqr_w), position_y + int(sqr_h))

                board_x = position_x//sqr_w
                board_y = position_y//sqr_h

                self.chess_board[board_y][board_x] = piece[0]

            piece_file_loc_black_bg = 'chess-com/chess-com-pieces/{}_B.JPG'.format(piece)

            #piece_file_loc = 'N.JPG'
            template = cv2.imread(piece_file_loc_black_bg,0)
            
            res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
            loc = np.where(res >= threshold)
            for pt in zip(*loc[::-1]):
                #looking for upper left corner of each square

                position_x = int((pt[0] // sqr_w) * sqr_w)
                position_y = int((pt[1] // sqr_h) * sqr_h)

                x_y_start = (position_x,position_y)
                x_y_end = (position_x + int(sqr_w), position_y + int(sqr_h))

                board_x = position_x//sqr_w
                board_y = position_y//sqr_h

                self.chess_board[board_y][board_x] = piece[0]
        
            self.board_to_fen()

    def board_to_fen(self):
        empty_squares = 0
        fen = ''
        chess_board = self.chess_board
        ##board is 2d chessboard, using fen notation and  E displays square
        for row in chess_board:
            for square in row:
                if (square == 'E'):
                    empty_squares+=1
                    continue
                else:
                    if (empty_squares != 0):
                        fen += str(empty_squares)
                        empty_squares = 0
                    fen += square
                
            if (empty_squares  != 0):
                fen += str(empty_squares)
                empty_squares = 0
            fen += '/'

        fen = fen[:-1]

        fen += self.side_to_move
        
        self.fen = fen

    
    def flip_fen(self):        
        split_fen = self.fen.split()[0]
        fliped_fen = split_fen[::-1]
        

        if self.current_bottom_side == 'W':
            self.current_bottom_side = 'B'
            self.side_to_move = self.black_to_move

        else :
            self.current_bottom_side = 'W'
            self.side_to_move = self.white_to_move

        self.fen = fliped_fen + self.side_to_move

    
    def get_fen_notation(self):
        return self.fen

    def get_image_path(self):
        return self.image_path
import tkinter as tk
from tkinter import filedialog

from PIL import ImageTk,Image
LARGE_FONT  = ("Verdana", 20)
MEDIUM_FONT = ("Verdana", 10)

class BoardTypePage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.controller = controller
        self.label_frame_1 = tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_1.pack(fill="x", padx = 5, pady=5)

        self.title_label = tk.Label(self.label_frame_1,text = 'Choose board type', font=LARGE_FONT,padx=10, pady=10)
        self.title_label.pack()

        self.label_frame_2 = tk.LabelFrame(self,width=250, height=255,borderwidth=0,highlightthickness=0)
        self.label_frame_2.pack(fill="x", padx = 5, pady=5)

        self.chess_com_img = Image.open("miscellaneous/chess-com-board.JPG")
        self.chess_com_img.thumbnail((185, 185),Image.ANTIALIAS)
        self.chess_com_img_resized = ImageTk.PhotoImage(self.chess_com_img)

        self.chess_com_board = tk.Button(self.label_frame_2, image = self.chess_com_img_resized, width = 185, height=185,relief=tk.FLAT, command = lambda : self.import_puzzle('CHESS_COM'))
        self.chess_com_board.pack(side = tk.LEFT, padx=70, pady=5)
        

        self.lichess_img = Image.open("miscellaneous/lichess-board.JPG")
        self.lichess_img.thumbnail((185, 185),Image.ANTIALIAS)
        self.lichess_img_resized = ImageTk.PhotoImage(self.lichess_img)

        self.lichess_board = tk.Button(self.label_frame_2, image = self.lichess_img_resized, width = 185, height=185,relief=tk.FLAT, command = lambda : self.import_puzzle('LICHESS'))
        self.lichess_board.pack(side = tk.RIGHT, padx=70, pady=5)

        self.label_frame_3 = tk.LabelFrame(self,width=250, height=255,borderwidth=0,highlightthickness=0)
        self.label_frame_3.pack(fill="x", padx = 5)

        self.chess_com_label = tk.Label(self.label_frame_3 ,text='Chess.com default board',font=MEDIUM_FONT)
        self.chess_com_label.pack(side =tk.LEFT,padx=75, pady=5)

        self.lichess_label = tk.Label(self.label_frame_3 ,text='Lichess.org default board',font=MEDIUM_FONT)
        self.lichess_label.pack(side =tk.RIGHT,padx=75, pady=5)


    def import_puzzle(self, board_type):
        filename = filedialog.askopenfile(initialdir = "/", title = "Select file",filetypes = (("jpeg files","*.JPG"),("png files","*.PNG*")) )
        self.controller.create_image_analyzer(board_type, filename.name)
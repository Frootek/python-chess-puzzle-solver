import tkinter as tk
from PIL import ImageTk,Image

LARGE_FONT  = ("Verdana", 20)
MEDIUM_FONT = ("Verdana", 10)
MEDIUM_MEDIUM_FONT = ("Verdana", 12)

window = tk.Tk()
window.title("Welcome to LikeGeeks app")
window.geometry("700x350")

label_frame_1 = tk.LabelFrame(window,width=250, height=125,borderwidth=0,highlightthickness=0)
label_frame_1.pack(fill="x", padx = 5, pady=5)

title_label = tk.Label(label_frame_1,text = 'Choose board type', font=LARGE_FONT,padx=10, pady=10)
title_label.pack()

label_frame_2 = tk.LabelFrame(window,width=250, height=255,borderwidth=0,highlightthickness=0)
label_frame_2.pack(fill="x", padx = 5, pady=5)

img = Image.open("miscellaneous/chess-com-board.JPG")
img.thumbnail((185, 185),Image.ANTIALIAS)
img_resized = ImageTk.PhotoImage(img)

chess_com_board = tk.Button(label_frame_2, image = img_resized, width = 185, height=185,relief=tk.FLAT)
chess_com_board.pack(side = tk.LEFT, padx=70, pady=5)

img2 = Image.open("miscellaneous/lichess-board.JPG")
img2.thumbnail((185, 185),Image.ANTIALIAS)
img_resized2 = ImageTk.PhotoImage(img2)

lichess_board = tk.Button(label_frame_2, image = img_resized2, width = 185, height=185,relief=tk.FLAT)
lichess_board.pack(side = tk.RIGHT, padx=70, pady=5)

label_frame_3 = tk.LabelFrame(window,width=250, height=255,borderwidth=0,highlightthickness=0)
label_frame_3.pack(fill="x", padx = 5)

chess_com_label = tk.Label(label_frame_3 ,text='Chess.com default board',font=MEDIUM_FONT)
chess_com_label.pack(side =tk.LEFT,padx=75, pady=5)

lichess_label = tk.Label(label_frame_3 ,text='Lichess.org default board',font=MEDIUM_FONT)
lichess_label.pack(side =tk.RIGHT,padx=75, pady=5)



window.mainloop()


import tkinter as tk
from puzzle_solver_frame import PuzzleSolverFrame

class Application(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        super().title("Puzzle-Solver by Frootek")
        
        self.geometry("450x650")

        self.frames = {}

        #add StartPage and MineSweeper to container
        frame = PuzzleSolverFrame(self,self)
        self.frames["PuzzleSolverFrame"] = frame
        frame.pack(fill='both')

        self.show_frame("PuzzleSolverFrame")


    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

app = Application()
app.mainloop()

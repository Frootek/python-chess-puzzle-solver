import tkinter as tk
from PIL import ImageTk,Image

from tkinter import filedialog
from tkinter import ttk

from stockfish import Stockfish
from image_analyzer import LichessImageAnalyzer, ChessComImageAnalyzer

import webbrowser
import threading

import time

from about_page import AboutPage
from board_type_page import BoardTypePage

"""

    Widgets for the puzzle_solver_frame
    NOTE: widget packing is done in layout

"""

LARGE_FONT  = ("Verdana", 20)


#0. label_frame widgets
class AboutBtn(tk.Button):
    def __init__(self,parent,controler):
        super().__init__(parent, text = 'About', command = lambda : self.show_about_page())

    def show_about_page(self):
        new_window = tk.Toplevel()
        new_window.geometry("1100x650")
        
        about_page = AboutPage(new_window, self)
        about_page.pack(fill='both')

        new_window.mainloop()


#1. label_frame widgets

""" class ImportBtn(tk.Button):
    def __init__(self,parent,controler):
        super().__init__(parent, text = 'Import puzzle', command = lambda : self.import_chess_puzzle())
        self.observers = []
        self.image_path = 'puzzles/chess-puzzle.JPG'
        self.image_analyzer = ImageAnalyzer(self.image_path)
        #self.notify()


    def add_observer(self,observer):
        self.observers.append(observer)

    def notify(self):
        for o in self.observers:
            o.update(self.image_analyzer)

    #open dialog box and get chess puzzle
    def import_chess_puzzle(self):
        #filename = tkFileDialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("jpeg files","*.jpg"),("all files","*.*")))
        filename = filedialog.askopenfile(initialdir = "/", title = "Select file",filetypes = (("jpeg files","*.JPG"),("png files","*.PNG*")) )
        
        self.image_analyzer = ImageAnalyzer(filename.name)
        self.notify() """

class ImportBtn(tk.Button):
    def __init__(self,parent,controler):
        super().__init__(parent, text = 'Import puzzle', command = lambda : self.import_chess_puzzle())
        self.observers = []
        self.image_path = ''
        self.image_analyzer = None
        self.new_window = None


    def add_observer(self,observer):
        self.observers.append(observer)

    def notify(self):
        for o in self.observers:
            o.update(self.image_analyzer)

    #open dialog box and get chess puzzle
    def import_chess_puzzle(self):
        #filename = tkFileDialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("jpeg files","*.jpg"),("all files","*.*")))
        self.new_window = tk.Toplevel()
        self.new_window.geometry("700x350")
        
        about_page = BoardTypePage(self.new_window, self)
        about_page.pack(fill='both')

        self.new_window.mainloop()
    
    def create_image_analyzer(self, board_type, file_location):
        print(board_type+ '  '+ file_location)
        self.new_window.destroy()

        if(board_type == 'CHESS_COM'):
            self.image_analyzer = ChessComImageAnalyzer(file_location)
            self.notify()            

        if(board_type == 'LICHESS'):
            self.image_analyzer = LichessImageAnalyzer(file_location)
            self.notify()






        

class ConfigureStockfishBtn(tk.Button):
    def __init__(self,parent,controler):
        super().__init__(parent, text = 'Configure Stockfish Location', command = lambda : self.import_stockfish())
        self.stockfish_location = ''
        self.observers = []

    def add_stockfish_location_observer(self,observer):
        pass

    def notify_stockfish_location_observers(self):
        for o in self.observers:
            o.update_stockfish_location(self.stockfish_location)

    def import_stockfish(self):
        filename = filedialog.askopenfile(initialdir = "/", title = "Select Stockfish .exe file" )
        self.stockfish_location = filename.name
        self.notify_stockfish_location_observers()


#2. label_frame widgets

class SideToMoveLabel(tk.Label):
    def __init__(self,parent,controler):
        self.OPTIONS = [
        "White to move",
        "Black to move",
        ]
        self.side = 0
        self.side_to_move = tk.StringVar(value = self.OPTIONS[self.side])
        super().__init__(parent,textvariable = self.side_to_move,padx=10, pady=10,relief="groove")
    
    def update(self,image_analyzer):
        self.side = 0
        self.side_to_move.set(self.OPTIONS[self.side])

    def flip_update(self):
        self.side = (self.side + 1 ) % 2
        self.side_to_move.set(self.OPTIONS[self.side])



###
#  W ->  A1 square is in bottom left
#  B ->  A1 square is u  
##3
class FlipBoard(tk.Button):
    def __init__(self,parent,controler):
        super().__init__(parent, text = 'Flip board', command = lambda : self.flip())
        self.flip_observers = []

    def add_flip_observers(self, observer):
        self.flip_observers.append(observer)

    def notify_flip_observers(self):
        for o in self.flip_observers:
            o.flip_update()
    
    def flip(self):
        self.notify_flip_observers()



#3. label_frame widgets

class FenEntry(tk.Entry):
    def __init__(self,parent,controler):
        self.fen_text_val = tk.StringVar(value="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
        super().__init__(parent,state='readonly', readonlybackground='SystemButtonFace', fg='black')
        super().config(textvariable=self.fen_text_val, relief='groove')
        self.fen_observers = []
        self.image_analyzer = None


    def update(self, image_analyzer):
        self.image_analyzer = image_analyzer
        new_fen = ''
        self.fen_text_val.set(image_analyzer.get_fen_notation())
        self.notify_fen_observers()

    def add_fen_observer(self, observer):
        self.fen_observers.append(observer)


    def notify_fen_observers(self):
        for o in self.fen_observers:
            o.fen_update(self.fen_text_val.get())

    def flip_update(self):
        self.image_analyzer.flip_fen()
        self.update(self.image_analyzer)
        self.notify_fen_observers()


#4. label_frame widgets

class ChessBoardImg(tk.Label):
    def __init__(self,parent,controler):
        # default chess puzzle
        self.chess_img = Image.open('puzzles/starting-position.JPG')
        self.chess_img = self.chess_img.resize((300,250), Image.ANTIALIAS)

        self.chess_img_resized = ImageTk.PhotoImage(self.chess_img)

        super().__init__(parent, image = self.chess_img_resized, width = 300, height=250)

    def update(self, image_analyzer):
        self.change_photo(image_analyzer.get_image_path())

    def change_photo(self, image_path):
        self.chess_img = Image.open(image_path)
        self.chess_img = self.chess_img.resize((300,250), Image.ANTIALIAS)
        self.chess_img_resized = ImageTk.PhotoImage(self.chess_img)
        super().config(image = self.chess_img_resized)



class AnalyzeBtn(tk.Button):
    def __init__(self, parent,controler):
        super().__init__(parent, text = 'Analyze', command = lambda : self.notify_analyze_observers())
        self.fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'
        self.analyze_observers = []

    #def update(self, image_analyzer):

    def fen_update(self, new_fen):
        self.fen = new_fen

    def add_analyze_observer(self,observer):
        self.analyze_observers.append(observer)
    
    def notify_analyze_observers(self):
        for o in self.analyze_observers:
            o.analyze_update(self.fen)



#4.5 label_frame_widgets

class AnalyzeProgressBar(ttk.Progressbar):
    def __init__(self, parent,controler):
        super().__init__(parent, orient = tk.HORIZONTAL,mode = 'determinate')

    def analyze_update(self, fen):
        progress_thread = threading.Thread(target = self.start)
        progress_thread.start()

    def start(self):
        self.pack(fill= 'x', pady='5')
        counter = 0
        self['value']= counter
        while counter<=100:
            self['value'] = counter
            time.sleep(0.5)
            counter += 10
        self['value'] = 0

        self.pack_forget()

#5. label_frame_widgets

class BestMove(tk.Label):
    def __init__(self,parent,controler):
        self.best_move = tk.StringVar(value ='e2e4' )

        super().__init__(parent,textvariable = self.best_move, font=LARGE_FONT,padx=10, pady=10)
        
        self.stockfish_location = 'C:\Program Files\Stockfish\stockfish.exe'
        self.stockfish = Stockfish(self.stockfish_location)  

    def analyze_update(self,fen):  
        analyze_thread = threading.Thread(target = self.analyze_fen,args=(fen,))
        analyze_thread.start()


    def update_stockfish_location(self, new_stockfish_location):
        self.stockfish = new_stockfish_location
    
    def update(self, image_analyzer):
        self.best_move.set('...')

    def analyze_fen(self,fen):
        self.stockfish.set_fen_position(fen)
        move = self.stockfish.get_best_move_time(5000)
        self.best_move.set(move) 

#6. label_frame_widgets

class LichesAnalysisBtn(tk.Button):
    def __init__(self, parent,controler):
        super().__init__(parent, text = 'Go to Liches Analysis', command = lambda : self.liches_analysis())
        self.fen = ''

    def fen_update(self, new_fen):
        self.fen = new_fen

    def liches_analysis(self):
        prefix = 'https://lichess.org/analysis/'
        url = prefix + self.fen.replace(' ', '_')
        webbrowser.open(url, new = 2)
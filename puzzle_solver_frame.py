import tkinter as tk
from puzzle_solver_frame_widgets import *

MEDIUM_FONT = ("Verdana", 10)
LARGE_FONT  = ("Verdana", 20)

BTN_DEF_HEIGHT = 10
BTN_DEF_WIDTH = 10

"""
    Contains app GUI layout    
"""



class PuzzleSolverFrame(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        #head title

        self.label_frame_0_1 =tk.LabelFrame(self,width=250, height=125,borderwidth=0,highlightthickness=0)
        self.label_frame_0_1.pack(fill="x", padx = 5, pady=5)

        self.title_label = tk.Label(self.label_frame_0_1,text = 'Puzzle-Solver', font=MEDIUM_FONT,padx=10, pady=10)
        self.title_label.pack()

        self.about_btn = AboutBtn(self.label_frame_0_1,self)
        self.about_btn.pack(side = tk.RIGHT,padx=5, pady=5)

        #first label_frame that contains import button

        self.label_frame_1 = tk.LabelFrame(self,width=250, height=80,borderwidth=0,highlightthickness=0)
        self.label_frame_1.pack(fill='x')

        self.import_btn = ImportBtn(self.label_frame_1,self)
        self.import_btn.pack(side = tk.LEFT, padx=10, pady=5)

        self.stockfish_location_btn = ConfigureStockfishBtn(self.label_frame_1,self)
        self.stockfish_location_btn.pack(side = tk.RIGHT, padx=10, pady=5)


        #second label_rame contains white/black to move options

        self.label_frame_2 = tk.LabelFrame(self, width=250, height=80,borderwidth=0,highlightthickness=0)
        self.label_frame_2.pack(fill='x')

        self.side_to_move = SideToMoveLabel(self.label_frame_2,self)
        self.side_to_move.pack(side=tk.LEFT,padx=10, pady=5)

        self.flip_btn = FlipBoard(self.label_frame_2,self)
        self.flip_btn.pack(side=tk.RIGHT,padx=10, pady=5)


        #third label_frame contains fen label and  notation

        self.label_frame_3 = tk.LabelFrame(self, width =250,height=80,borderwidth=0,highlightthickness=0,pady=5)
        self.label_frame_3.pack(fill='x')

        self.fen_label = tk.Label(self.label_frame_3, text='FEN :')
        self.fen_label.pack(side = tk.LEFT, padx=10)

        self.fen_entry_box = FenEntry(self.label_frame_3,self)
        self.fen_entry_box.pack(side=tk.LEFT, fill = 'x',expand=True,padx=10)

        #fourth label_frame contains image and analyze btn

        self.label_frame_4 = tk.LabelFrame(self,width =250,height=80,borderwidth=0,highlightthickness=0, pady=5)
        self.label_frame_4.pack()

        self.chess_board_img = ChessBoardImg(self.label_frame_4,self)
        self.chess_board_img.pack()

        self.analyze_btn = AnalyzeBtn(self.label_frame_4,self);
        self.analyze_btn.pack(fill='x', pady='5')

        self.progress_bar = AnalyzeProgressBar(self.label_frame_4,self)


        #fifth label_frame contains best_move_label and best move

        self.label_frame_5 = tk.LabelFrame(self,width =250,height=80,borderwidth=0,highlightthickness=0, pady=5)
        self.label_frame_5.pack()

        self.best_move_label = tk.Label(self.label_frame_5,text='Best move :',font=LARGE_FONT)
        self.best_move_label.pack(side = tk.LEFT,padx=10)

        self.best_move = BestMove(self.label_frame_5,self)
        self.best_move.pack()

        #6. label_frame contains liches_analysis btn
        self.label_frame_6 = tk.LabelFrame(self,width =250,height=80,borderwidth=0,highlightthickness=0, pady=5)
        self.label_frame_6.pack()
        
        self.lichess_btn = LichesAnalysisBtn(self.label_frame_6,self)
        self.lichess_btn.pack()


        self.configure_widgets()



    def configure_widgets(self):
        ### DESIGN - PATTERN - OBSERVER ###
        # subject -> import_btn
        # observers -> fen_entry_box , chess_board_img, best_move, side_to_move
        ### !--------------------------> ###
        self.import_btn.add_observer(self.fen_entry_box)
        self.import_btn.add_observer(self.chess_board_img)
        self.import_btn.add_observer(self.best_move)
        self.import_btn.add_observer(self.side_to_move)
        ###
        # subject  -> fen_entry_box
        # observers -> analyze_btn, liches_analysis
        ###
        self.fen_entry_box.add_fen_observer(self.analyze_btn)
        self.fen_entry_box.add_fen_observer(self.lichess_btn)
        ###
        # subject -> analyze_btn
        # observers -> best_move, progress_bar
        ###
        self.analyze_btn.add_analyze_observer(self.best_move)
        self.analyze_btn.add_analyze_observer(self.progress_bar)
        ###
        # subject -> flip_btn
        # observers -> fen_entry_box
        ###
        self.flip_btn.add_flip_observers(self.fen_entry_box)
        self.flip_btn.add_flip_observers(self.side_to_move)
import tkinter as tk
from PIL import ImageTk,Image

SMALL_FONT = ("Verdana", 5)
MEDIUM_FONT = ("Verdana", 10)
MEDIUM_MEDIUM_FONT = ("Verdana", 12)
LARGE_FONT = ("Verdana", 20)

class AboutPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.label_frame_1 = tk.LabelFrame(self,width=250, height=125,borderwidth=1,highlightthickness=1)
        self.label_frame_1.pack(fill="x", padx = 5, pady=5)

        self.img = Image.open("miscellaneous/portrait.png")
        self.img.thumbnail((150, 150),Image.ANTIALIAS)
        self.img_resized = ImageTk.PhotoImage(self.img)

        self.portrait_img = tk.Label(self.label_frame_1, image = self.img_resized, width = 150, height=150)
        self.portrait_img.pack(side = tk.LEFT, padx=5, pady=5)

        self.information_label = tk.LabelFrame(self.label_frame_1,width=250, height=125, borderwidth=0 ,highlightthickness=0)
        self.information_label.pack(side=tk.LEFT, padx = 5, pady=5)

        self.name_frame = tk.LabelFrame(self.information_label,width=250, height=125,borderwidth=0 ,highlightthickness=0)
        self.name_frame.pack(padx = 5, pady=5,fill='both')

        self.name_label = tk.Label(self.name_frame,text='Name : Marin Jurić',font=MEDIUM_FONT)
        self.name_label.pack(side =tk.LEFT)

        self.age_frame = tk.LabelFrame(self.information_label,width=250, height=125,borderwidth=0 ,highlightthickness=0)
        self.age_frame.pack(padx = 5, pady=5,fill='both')

        self.age_label = tk.Label(self.age_frame,text='Age   : 22',font=MEDIUM_FONT)
        self.age_label.pack(side =tk.LEFT)


        self.country_frame = tk.LabelFrame(self.information_label,width=250, height=125,borderwidth=0 ,highlightthickness=0)
        self.country_frame.pack(padx = 5, pady=5,fill='both')

        self.counter_label = tk.Label(self.country_frame,text='Country : Croatia (Hrvatska) ',font=MEDIUM_FONT)
        self.counter_label.pack(side =tk.LEFT)

        self.faculty_frame = tk.LabelFrame(self.information_label,width=250, height=125,borderwidth=0 ,highlightthickness=0)
        self.faculty_frame.pack(padx = 5, pady=5,fill='both')

        self.faculty = tk.Label(self.faculty_frame,text='University of Zagreb, Faculty of Electrical Engineering and Computing',font=MEDIUM_FONT)
        self.faculty.pack(side =tk.LEFT)

        self.label_frame_2 = tk.LabelFrame(self,width=250, height=325,borderwidth=1,highlightthickness=1, text = "About the App", font=MEDIUM_FONT)
        self.label_frame_2.pack(fill="x", padx = 5, pady=5)

        self.about_app_text = """
        Hello everyone!
        Recently I have been solving alot of chess puzzles on liches.org.
        Whenever I hit a dead-end at a certain puzzle (meaning I just could not figure out the answer of that position).
        I always had the option of failing the puzzle or setting up the analysis board by hand which can be cumbersome at times.

        With this tool you can just snip chess puzzle on lichess.org or chess.com.
        (However, because of template matching you can only use default liches and default chess.com board)
        Use program like snipping tool on Windows to grab chess_board, import jpeg or png  and then simply click analyze!

        One important thing to note that this program DOES NOT ship stockfish engine, but rather
        uses wrapper module so you can use any stockfish version you have.

        By default, program looks for a stockfish.exe file at 'C:\Program Files\Stockfish\stockfish.exe'
        If stockfish is not installed there,  you must configure stockfish location before running the analysis.
        Have fun!
        """
        self.T = tk.Text(self.label_frame_2, font=MEDIUM_MEDIUM_FONT,background="SystemButtonFace",relief=tk.FLAT)
        self.T.pack(fill='both',padx=5,pady=5)
        self.T.insert(tk.END, self.about_app_text)
        self.T.config(state=tk.DISABLED)